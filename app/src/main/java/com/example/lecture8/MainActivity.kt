package com.example.lecture8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        logInButton.setOnClickListener {
            if (emailEditText.text!!.isEmpty()) return@setOnClickListener Toast.makeText(
                this,
                "გთხოვთ შეიყვანოთ მეილი",
                Toast.LENGTH_SHORT
            ).show()
            if (passwordEditText.text!!.isEmpty()) return@setOnClickListener Toast.makeText(
                this,
                "გთხოვთ შეიყვანოთ პაროლი",
                Toast.LENGTH_SHORT
            ).show()
            if (isEmailValid(emailEditText.text.toString())) {
                Toast.makeText(this, "მოდი მოდი ეხლა დავსხედით", Toast.LENGTH_LONG).show()
            } else Toast.makeText(this, "რანაირი მეილია რაარი", Toast.LENGTH_LONG).show()
        }

        forgotPasswordTextView.setOnClickListener {
            Toast.makeText(this, "რანაირად დაგავიწდა? :D", Toast.LENGTH_LONG).show()
        }

        signUpTextView.setOnClickListener {
            Toast.makeText(this, "ადმინ ადმინია რად უნდა რეგისტრაცია", Toast.LENGTH_LONG).show()
        }
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    // Or REGEX

    //private val emailRegex = compile(
    //        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
    //                "\\@" +
    //                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
    //                "(" +
    //                "\\." +
    //                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
    //                ")+")

}
